from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from .models import complaints, complaintRemark, category, subcategory
from django.db.models import Count
from django.contrib.auth.models import User
import datetime 
@login_required(login_url="/login/")
def index(request):
    
    context = {}
    context['complaints'] = complaints.objects.all()
    context['not_processed'] = context['complaints'].filter(status="pending").values_list('lastUpdationDate__year', 'lastUpdationDate__month').annotate(Count('id')).order_by('lastUpdationDate__year', 'lastUpdationDate__month')
    context['inprocess'] = context['complaints'].filter(status="InProcess").values_list('lastUpdationDate__year', 'lastUpdationDate__month').annotate(Count('id')).order_by('lastUpdationDate__year', 'lastUpdationDate__month')
    context['resolved'] = context['complaints'].filter(status="resolved").values_list('lastUpdationDate__year', 'lastUpdationDate__month').annotate(Count('id')).order_by('lastUpdationDate__year', 'lastUpdationDate__month')
    
        
    context['segment'] = 'index'

    html_template = loader.get_template( 'index.html' )
    return HttpResponse(html_template.render(context, request))


# @login_required(login_url="/login/")
# def pages(request):
#     context = {}
#     # All resource paths end in .html.
#     # Pick out the html file name from the url. And load that template.
#     try:
        
#         load_template      = request.path.split('/')[-1]
#         context['segment'] = load_template
#         html_template = loader.get_template( load_template )
#         return HttpResponse(html_template.render(context, request))
        
#     except template.TemplateDoesNotExist:

#         html_template = loader.get_template( 'page-404.html' )
#         return HttpResponse(html_template.render(context, request))

#     except:
    
#         html_template = loader.get_template( 'page-500.html' )
#         return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def addcategory(request):
    context = {}
    context['categories'] = category.objects.all()
    context['subcategories'] = subcategory.objects.all()
    if request.POST:
        if 'cat' in request.POST:
            cat = category.objects.get(categoryName=request.POST.get('cat'))
            add_subcat = subcategory(
                        categoryName = cat,
                        subcategory = request.POST.get('name'),
                        creationDate = datetime.datetime.now()
                    ).save()
            context['message'] ="{} created succesfully".format(add_subcat.subcategory)
        else:
            add_cat = category(
                            categoryName = request.POST.get('name'),
                            categoryDescription = request.POST.get('descp'),
                            creationDate = datetime.datetime.now()
                            ).save()
            context['message'] ="{} created succesfully".format(add_cat.categoryName)                 

    return render(request, 'forms.html', context)


@login_required(login_url="/login/")
def logComplaint(request):
    context = {}
    if request.user.is_superuser:
        context['complaints'] = complaints.objects.all()
    else:
        context['complaints'] = complaints.objects.filter(userId__id=request.user.id)  
    context['subcategories'] = subcategory.objects.all()
    return render(request, 'logComplaint.html', context)

# @login_required(login_url="/login/")
def manageUsers(request):
    context = {}
    context['users'] = User.objects.all()
    return render(request, 'edit-user.html', context)