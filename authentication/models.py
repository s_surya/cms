from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):
    user= models.OneToOneField(User,  related_name="related_user",on_delete=models.CASCADE)
    contactNo = models.CharField(max_length=15, default='', null=True, blank=True)
    address = models.CharField(max_length=500, default='', null=True, blank=True)
    Location = models.CharField(max_length=500, default='', null=True, blank=True)
    country = models.CharField(max_length=500, default='', null=True, blank=True)
    empcode = models.CharField(max_length=500, default='', null=True, blank=True)
    userImage = models.ImageField(upload_to='images/user/', null=True, blank=True)
    regDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    updationDate =  models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.user.first_name