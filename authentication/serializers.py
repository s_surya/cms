from rest_framework import serializers
from .models import UserProfile
from django.contrib.auth.models import User


class userSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')

class UserProfileSerializer(serializers.ModelSerializer):
    user = userSerializer(read_only=False)
    class Meta:
        model = UserProfile
        fields = (
            'user',
            'contactNo',
            'address',
            'Location',
            'country',
            'empcode',
            'userImage',
            'regDate',
            'updationDate',
        )        