


from django.contrib import admin
from django.urls import path, include  # add this
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static
from django.conf import settings
admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),          # Django admin route
    path("", include("authentication.urls")), # Auth routes - login / register
    path("", include("app.urls")),             # UI Kits Html files
    path("api/", include("api.urls"))
]
urlpatterns += staticfiles_urlpatterns()
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
